# C Unit Testing Example Using Rust/Cargo

This depends on the `cc` Cargo package.

Use `cargo test` to run the unit tests. The tests are written in C and driven with Rust.
