#ifndef TEST_FOO_H
#define TEST_FOO_H

// Strictly unit testing functions
void test_add(int * const);
void test_bad_add(int * const);

#endif
