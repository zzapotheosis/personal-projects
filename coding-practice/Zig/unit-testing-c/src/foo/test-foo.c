#include "test-foo.h"
#include "foo.h"

// Unit tests
void test_foo_test_add(int * const result) {
  *result = 1; // Assume fail
  int a = 10;
  int b = 5;
  if (add(a, b) == 15)
    *result = 0;
}

void test_foo_test_bad_add(int * const result) {
  *result = 1; // Assume fail
  int a = 10;
  int b = 5;
  if (bad_add(a, b) == 15)
    *result = 0;
}

