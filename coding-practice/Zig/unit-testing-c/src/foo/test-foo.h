#ifndef TEST_FOO_H
#define TEST_FOO_H

// Strictly unit testing functions
void test_foo_test_add(int * const);
void test_foo_test_bad_add(int * const);

#endif
