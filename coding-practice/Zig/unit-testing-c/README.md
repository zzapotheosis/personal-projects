# Unit Testing C with Zig

This is an example of using Zig's built-in unit testing framework to unit test C code.

Use `zig build test` to execute the unit tests. We expect one test to pass and one to fail.
