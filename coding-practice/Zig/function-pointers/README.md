# Function Pointers in Zig

Technically, there is no such thing as a "function pointer" in Zig. This example is to help experienced C programmers understand how
function pointers in C might translate to something similar in Zig.
